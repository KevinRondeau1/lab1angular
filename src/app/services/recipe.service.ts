import { Injectable } from '@angular/core';
import { Recipe } from '../models/recipe.model';
import { AuthService } from 'src/app/services/auth.service';
import { Observable } from 'rxjs';
import { MarthaRequestService } from './martha-request.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  private _currentRecipe : Recipe | null = null;

  get currentRecipe(): Recipe | null {
    return this._currentRecipe;
  }

  constructor(private authService: AuthService,private martha:MarthaRequestService) {
  }

  getRecipes():Observable<Recipe[]|null>{
    return this.martha.select('recipes-list', this.authService.currentUser).pipe(
      map(data => {
        console.log('recipeService', data);
        if (data) {
        return data
        } else {
         return null
        }
      })
    );
  }

  getRecipe(recipeId: string):Observable<Recipe>{
    return this.martha.select('recipes-read', {"id":recipeId,"email":this.authService.currentUser?.email}).pipe(
      map(data => {
        console.log('recipeService', data);
        if (data?.length==1) {
        return data[0]
        } else {
         return null
        }
      })
    );
  }


  createRecipe(recipe: Recipe): Observable<boolean> {
    console.log('recipeService');
    recipe.email= this.authService.currentUser!.email
    recipe.id = Math.random().toString(16).substring(2);
    return this.martha.insert('recipes-create', recipe).pipe(
          map(result => {
            console.log('recipeService', result);
            if (result?.success) {
              console.log("recipeService added ",recipe)
              return true;
            } else {
              return false;
            }
          })
        );
  }
  updateRecipe(recipe: Recipe): Observable<boolean> {
    console.log('recipeService');

    return this.martha.insert('recipes-update', recipe).pipe(
          map(result => {
            console.log('recipeService', result);
            if (result?.success) {
              console.log("recipeService added ",recipe)
              return true;
            } else {
              return false;
            }
          })
        );
  }

  deleteRecipe(removed:Recipe):Observable<boolean>{
    console.log('recipeService');
    return this.martha.insert('recipes-delete', removed).pipe(
          map(result => {
            console.log('recipeService', result);
            if (result?.success) {
              console.log("recipeService deleted ",removed)
              return true;
            } else {
              return false;
            }
          })
        );
  }

}
