import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/app/services/auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  authError: string | null = null;

  loginForm: FormGroup;
  constructor(private authService: AuthService, private router: Router,private translate:TranslateService)  {

    this.loginForm=new FormGroup({
      email:new FormControl('',[Validators.required,Validators.email] ),
      password: new FormControl(null, [Validators.required, Validators.minLength(3)]),
    });
   }

  ngOnInit(): void {
  }

  logIn(){
    this.authService.logIn(this.loginForm.value).subscribe(success => {
      console.log('Login component', success);

      if (success) {
        this.router.navigate(['/recipes']);
      } else {
        this.authError = "Invalid credentials!";
      }
    });
  }
}
