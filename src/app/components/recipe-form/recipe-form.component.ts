import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Recipe } from 'src/app/models/recipe.model';
import { RecipeService } from 'src/app/services/recipe.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-recipe-form',
  templateUrl: './recipe-form.component.html',
  styleUrls: ['./recipe-form.component.css']
})
export class RecipeFormComponent implements OnInit {
  recipeForm:FormGroup;
  id !: string | null;

  constructor(private recipeService: RecipeService,private authService: AuthService,private router:Router,private route: ActivatedRoute) {
    this.recipeForm=new FormGroup({
    name: new FormControl(null, [Validators.required, Validators.maxLength(20),Validators.minLength(3)]),
    category: new FormControl(0, [Validators.required]),
    description: new FormControl(null, [Validators.required,Validators.maxLength(50),Validators.minLength(3)])
  });
}

  ngOnInit(): void {
    this.id=this.route.snapshot.paramMap.get('id');
    console.log(this.id);
    if (this.id!=null)
    {
      let currentRecipe:Recipe
      console.log("Here")
      this.recipeService.getRecipe(this.id).subscribe(success=>{
        if (success) {
          currentRecipe=success as Recipe
          const {id,email,...recipeInfo}=currentRecipe
          this.recipeForm.setValue(recipeInfo)
          // this.recipeForm.controls['name'].setValue(currentRecipe.name)
        } else {
          console.log("invalid recipe");
        }
      }
      )

    }
  }

  submitRecipe() {
    if (this.id==null)
    {
      let newRecipe = new Recipe(this.recipeForm.value)
      this.recipeService.createRecipe(newRecipe).subscribe(success => {
          console.log('newRecipe component', success);

          if (success) {
            this.router.navigate(['/recipes']);
          } else {
            console.log("Invalid credentials!");
          }
        });
        this.recipeForm.reset()

      }else{
        let newRecipe = new Recipe(this.recipeForm.value)
        newRecipe.id=this.id
        newRecipe.email=this.authService.currentUser!.email

        this.recipeService.updateRecipe(newRecipe).subscribe(success => {
          console.log('newRecipe component', success);

          if (success) {
            this.router.navigate(['/recipes']);
          } else {
            console.log("Invalid credentials!");
          }
        });
        this.recipeForm.reset()
      }

      }



}
