import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Recipe } from 'src/app/models/recipe.model';

@Component({
  selector: '[app-recipes-list-item]',
  templateUrl: './recipes-list-item.component.html',
  styleUrls: ['./recipes-list-item.component.css']
})
export class RecipesListItemComponent implements OnInit {
  @Input() recipe!: Recipe;
  @Output() recipeDeleted = new EventEmitter();
  @Output() recipeEdit = new EventEmitter();
  constructor() {  }

  ngOnInit(): void {
  }

  recipeCategory(string:string){
    if (this.recipe.category==0)
      return "Breakfast"
    if (this.recipe.category==1)
      return "Lunch"
    if (this.recipe.category==2)
      return "Dinner"

    return null
  }
  delete(){
    console.log("1st")
    this.recipeDeleted.emit(this.recipe);
  }
  edit(){
    console.log("1st")
    this.recipeEdit.emit(this.recipe);
  }
}
