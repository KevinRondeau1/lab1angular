import { Routes } from "@angular/router";
import { LoginComponent } from "../login/login.component";
import { SignupComponent } from "../signup/signup.component";
import { RecipesComponent } from '../recipes/recipes.component';
import { ProfileComponent } from '../profile/profile.component';
import { WildcardComponent } from '../wildcard/wildcard.component';
import { ProfileDetailsComponent } from "../profile-details/profile-details.component";
import { AuthGuard } from "src/app/guards/auth.guard";
import { LoggedInGuard } from '../../guards/loggedIn.guard';
import { RecipeFormComponent } from "../recipe-form/recipe-form.component";
export const routes: Routes = [
  { path: '', canActivateChild: [LoggedInGuard], children: [
    { path: '', component: LoginComponent },
    { path: 'signup', component: SignupComponent },
  ]},
  { path: '', canActivateChild: [AuthGuard], children: [
    { path: 'recipes', component: RecipesComponent },
    { path: 'recipe/new', component: RecipeFormComponent },
    { path: 'recipe/:id', component: RecipeFormComponent },
    { path: 'profile', component: ProfileComponent },
    { path: 'profile/:email', component: ProfileDetailsComponent }
  ]},
  {path: '**',component:WildcardComponent},
];
