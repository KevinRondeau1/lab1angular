import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  language!:string|null

  private readonly LANGUAGE_KEY = 'recipeasy.language';

  constructor(private authService: AuthService,private router: Router,private translate:TranslateService) {
    this.translate.addLangs(['en', 'fr']);
    this.translate.setDefaultLang('en');
    this.language=localStorage.getItem(this.LANGUAGE_KEY)??'en';
    console.log(this.language);
    this.translate.use(this.language);
   }

   ngOnInit(): void{
  }

  logOut(){
    this.authService.logOut();
    this.router.navigate(['/']);
  }

  changeLanguage(langEvent: EventTarget | null){
    if(langEvent instanceof HTMLSelectElement)
    {
      let lang = (langEvent as HTMLSelectElement).value;
      this.translate.use(lang);
      localStorage.setItem(this.LANGUAGE_KEY, lang);
    }
  }

  getUser(){
    if(this.authService.isLoggedIn){
      return true;
    }else{
      return false;
    }
  }

}
