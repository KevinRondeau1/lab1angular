import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { Recipe } from 'src/app/models/recipe.model';
import { RecipeService } from '../../services/recipe.service';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: '[app-recipes-list]',
  templateUrl: './recipes-list.component.html',
  styleUrls: ['./recipes-list.component.css']
})

export class RecipesListComponent implements OnInit {
  @Input() recipes!: Recipe[];
  @Output() recipeDeletion = new EventEmitter();
  @Output() recipeEdition = new EventEmitter();



  constructor(private recipeService: RecipeService,private router:Router, private authService:AuthService) {
  }

  ngOnInit(): void {
  }

  recipeDeleted(removed:Recipe) {
    console.log("2nd")
    this.recipeDeletion.emit(removed);
  }
  recipeEdit(edit:Recipe) {
    console.log("2nd")
    this.recipeEdition.emit(edit);
  }
}
