import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Recipe } from 'src/app/models/recipe.model';
import { RecipeService } from '../../services/recipe.service';
import {RecipesListComponent } from '../recipes-list/recipes-list.component';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit {
  recipes : Recipe[] = [];
  getRecipes() {
    this.recipeService.getRecipes().subscribe(success => {
      console.log('Login component', success);
      if (success) {
        this.recipes=success as Recipe[]
      } else {
        console.log("couldnt load recipes")
      }
    });
  }

  constructor(private recipeService:RecipeService,private authService:AuthService,private router:Router) {
   }

  ngOnInit(): void {
    this.getRecipes()
  }
  recipeDelete(removed:Recipe) {
    console.log(removed.email)
    removed.email=this.authService.currentUser!.email
    this.recipeService.deleteRecipe(removed).subscribe(success => {
      console.log('deleted recipe', success);

      if (success) {
        console.log("recipeDeleted")
        this.getRecipes()
      } else {
        console.log("Error, Couldnt Delete Recipe")
      }
    });
  }
  recipeEdit(edit:Recipe) {
    this.router.navigate(['/recipe',edit.id]);
  }
}
